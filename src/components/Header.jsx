import React from "react";
import HighlightIcon from "@material-ui/icons/Highlight";

function Header() {
  return (
    <header>
      <h1>
        <HighlightIcon />
          To do List de Catherine
      </h1>
    </header>
  );
}

export default Header;
