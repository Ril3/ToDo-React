import React, { useState } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Note from "./Note";
import CreateArea from "./CreateArea";
import { useEffect } from "react";

function App() {
  const [notes, setNotes] = useState([]);

  function addNote(newNote) {
    setNotes(prevNotes => {
      return [...prevNotes, newNote];
    });
    localStorage.setItem("notes", JSON.stringify([...notes, newNote]));
  }

  function deleteNote(id) {
    setNotes(prevNotes => {
      return prevNotes.filter((noteItem, index) => {
        return index !== id;
      });
    });
    localStorage.setItem("notes", JSON.stringify(notes.filter((noteItem, index) => {
      return index !== id;
    })))
  }
    
  
  useEffect(()=>{
    if (localStorage.length!==0) {
      setNotes(JSON.parse(localStorage.getItem('notes')));
    }
  }, [])
  

  return (
    <div>
      <Header />
      <CreateArea onAdd={addNote} />
      {notes.map((noteItem, index) => {
        return (
          <Note
            key={index}
            id={index}
            title={noteItem.title}
            content={noteItem.content}
            onDelete={deleteNote}
          />
        );
      })}
      <Footer />
    </div>
  );
}

export default App;
